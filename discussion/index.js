/* Javascript Synchronous and Asynchronous
    JS by default is synchronous
        > it means that only one statement is executed at a time.
    Asynchronous
        > means that we can proceed to execute other statements/code block, while time consuming code is running in the background.
*/


/* Getting all POST
    >the Fetch API allows you to asynchronously request for a resources(data).*/

 /*  "promise" is an object that represent the eventual completion (or failure) of an asynchronous function and it's resulting value.
    Syntax:
        fetch("URL");
*/

/* Promise
    > may be in one of the 3 possible states:
        1. full filled
        2. Rejected
        3. Pending

    Syntax:
      fetch("URL")
        .then((response)=>{//line code})
 */
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
/*By using the response.status we only check if the promise is full-filled or rejected.*/

/* "".then" method captures the response object and returns another "promise" which will eventually be resolved or rejected*/
// .then((response) => console.log(response.status))


/* use the "json" from the response object to convert the data retrieved into JSON format to be used in our application*/
.then((response) => response.json())

/*using multiple "then" method that creates a promise chain*/
.then((json) => console.log(json))


/*Display the title of the post */

.then((json) => {
    json.forEach(posts => console.log(posts.title))
})

async function fetchData(){
    
    /*await
        > waits for the "fetch" method to complete then it stores the value in the result variable
    */
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")
    /* result 
        >returned by fetch is a promise made
     */
    console.log(result)
    console.log(typeof result)
    console.log(result.body)

    let json = await result.json()
    console.log(json)
}

fetchData();

/* Getting a specific post
    > retrieve, /post/:id, GET
*/

/*:id
    >is a wildcard where you can put the unique identifier as value to show specific data/resources */
fetch("https://jsonplaceholder.typicode.com/posts/10")
.then((response) => response.json())
.then((json) => console.log(json))

/*Creating a post
    Syntax:
        fetch('URL, {options})
        .then((response) => {line code})
        .then((line code) => {line code} / console.log(line code))
*/

fetch("https://jsonplaceholder.typicode.com/posts", {method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
//Sets the content/body data of the "Request" object to be sent to the backend/server
    body: JSON.stringify({
        title: "New Post",
        body: "Hello Batch 197",
        userId: 1
    })
})
//response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))

/* Updating a post
    >(update,/posts/:id, PUT)
    > used to update the whole object/document
 */

    fetch("https://jsonplaceholder.typicode.com/posts/10", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Updated Post",
            body: "Hello again Batch 197!",
            userId: 1
        })
    })
.then((response) => response.json())
.then((json) => console.log(json))

/*Patch
 */

fetch("https://jsonplaceholder.typicode.com/posts/10", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Corrected post"
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

/* Deleting a post
    (delete,/post/:id, DELETE)
*/
fetch("https://jsonplaceholder.typicode.com/posts/10", {
    method: "DELETE"
})

