
async function getToDos(){
    
  const response = await fetch("https://jsonplaceholder.typicode.com/todos")

  const resJson = await response.json()
  console.log(resJson)
  const myTitles = resJson.map((json) => 'title' + json.title)
  console.log(myTitles)

}
getToDos();

//GET
fetch("https://jsonplaceholder.typicode.com/todos/15")
.then((response) => response.json())
.then((json) => console.log(json))

//List title and Completed
fetch("https://jsonplaceholder.typicode.com/todos/15")
.then((response) => response.json())
.then((json) => console.log(`title: ${json.title} | completed
: ${json.completed}`))

//POST
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
    },
    body: JSON.stringify({
    title: "New Vitamin",
    completed: false,
    userId: 6
    })
})
.then((response) => response.json())
.then((json) => console.log(json))


//PUT
fetch("https://jsonplaceholder.typicode.com/todos/15", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
    },
    body: JSON.stringify({
    title: "Bentley",
    description: "British Company that designs, develops and manufactures luxury motorcars",
    status: "In Progress",
    dateCompleted: 1910,
    userId: 6
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

//PATCH
fetch("https://jsonplaceholder.typicode.com/todos/15", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
    },
    body: JSON.stringify({
    status: "Completed",
    dateCompleted: 1919,
    })
})
.then((response) => response.json())
.then((json) => console.log(json))


//DELETE
fetch("https://jsonplaceholder.typicode.com/todos/15", {
  method: "DELETE",
})



// using the asynchronous method
// console.log(' ')
console.log('Using the Asynchronous method')


//POST
async function createToDos(){
  const response = await fetch ("https://jsonplaceholder.typicode.com/todos",{
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      userId: 6,
      completed: false,
      title: "New Cars"
    })
  })
  const reJson = await response.json()
  console.log(reJson)
}
createToDos()

//PUT

async function addToDos(){
  const response = await fetch ("https://jsonplaceholder.typicode.com/todos/105",{
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      title: "Alfa Romeo",
      description: "The company has been involved in car racing since 1911",
      status: "In Progress",
      dateCompleted: "1910",
      userId: 6
    })
  })
  const reJson = await response.json()
  console.log(reJson)
}
addToDos()

//PATCH

async function upToDoes(){
  const response = await fetch ("https://jsonplaceholder.typicode.com/todos/105", {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      status: "Completed",
      dateCompleted: "1915"
    })
  })
  const reJson = await response.json()
  console.log(reJson)
}
upToDoes()

//DELETE

async function delToDos(){
  const response = await fetch ("https://jsonplaceholder.typicode.com/todos/105", {
    method: "DELETE"
  })
  const reJson = await response.json()
  console.log(reJson)
  console.log("Deleted the ID 105")
}
delToDos()
